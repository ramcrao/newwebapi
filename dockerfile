# Define base image
FROM mcr.microsoft.com/dotnet/core/sdk:latest AS build

# Copy project files
RUN mkdir -p /app
WORKDIR /app
COPY *.sln .
COPY WebApplication1/*.csproj ./WebApplication1/
COPY packages/. ./packages/
#RUN dotnet restore "/source/CoreWebAPI/src/CoreWebAPI/CoreWebAPI.xproj"
#RUN dotnet restore "/app/CoretestingCI/CoretestingCI.sln"
RUN dotnet restore 
COPY WebApplication1/. ./WebApplication1/

WORKDIR /app/
RUN dotnet publish -c Release -o out
#COPY ["CoreWebAPI/CoreWebAPI.sln", "./CoreWebAPI/CoreWebAPI.sln"]

# Copy all source code
#COPY CoreWebAPI/. .

# Restore


# Publish
#WORKDIR /source/src
#RUN dotnet publish -c Release -o /publish

# Runtime
#FROM microsoft/dotnet:2.2-aspnetcore-runtime
#WORKDIR /publish
#COPY --from=build-env /publish .
#ENTRYPOINT ["dotnet", "CoreWebAPI.dll"]
